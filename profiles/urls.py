from django.urls import path

from .views import registrar_user, login_user

urlpatterns = [
    path('', registrar_user, name='register_user'),
    path('login_user/', login_user, name='login_user')
]
