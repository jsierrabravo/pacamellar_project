# Create your views here.
from django.contrib.auth import authenticate, login
from django.http import HttpResponse
from django.shortcuts import render, redirect

from .forms import CreateUserForm


def registrar_user(request):
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        # print(type(request.POST["option"]))
        if form.is_valid():
            # print('paso')
            form.save()
            return redirect('register_user')

    else:
        form = CreateUserForm()
    context = {'form': form}
    return render(request, 'sign_up.html', context)
    # return render(request, 'necessities_list.html', context)


def login_user(request):
    msg = ' '
    if request.method == 'POST':
        username = request.POST["username"]
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            # print('user: ', user.values())
            # print('type: ', type(user))
            login(request, user)
            # context = {
            #     'user': request.user
            # }
            # return render(request, 'index.html')#, context)
            return redirect('/')
        else:
            msg = 'User name or password incorrect'
    context = {'msg': msg}
    return render(request, 'sign_up.html', context)
