from django import forms

from .models import Employee, Employer, Skill, DocumentType, City
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm


class CreateUserForm(UserCreationForm):
    CHOICES = [("1", 'Empleado'), ("2", 'Empleador')]

    name = forms.CharField(required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    lastname = forms.CharField(required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    document_type = forms.ModelChoiceField(queryset=DocumentType.objects.all(), widget=forms.Select(attrs={'class': 'form-control','style':'padding-bottom: 0px;padding-top: 0px;'}))
    document_number = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control'}))
    age = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control'}))
    phone_number = forms.CharField(required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    email= forms.EmailField(required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    # qualification = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control'}))
    city = forms.ModelChoiceField(queryset=City.objects.all(), widget=forms.Select(attrs={'class': 'form-control mb-3', 'style':'padding-bottom: 0px;padding-top: 0px;'}))
    skill= forms.ModelChoiceField(queryset=Skill.objects.all(), widget=forms.Select(attrs={'class': 'form-control','style':'padding-bottom: 0px;padding-top: 0px;'}))
    user_name= forms.CharField(required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    option = forms.ChoiceField(choices=CHOICES, widget=forms.Select(attrs={'class': 'form-control'}))

    class Meta:
        model = User
        fields = ['name', 'lastname', 'document_type', 'document_number', 'age', 'phone_number', 'email', 'city', 'skill','user_name','password1', 'password2', 'option']

    def save(self, commit=True):
        new_user=User.objects.create_user(
            username=self.cleaned_data["user_name"],
            password=self.cleaned_data['password1']
        )
        if self.cleaned_data['option']== "1":
            new_empleado=Employee.objects.create(
                user=new_user,
                name=self.cleaned_data["name"],
                lastname=self.cleaned_data["lastname"],
                document_type=self.cleaned_data["document_type"],
                document_number=self.cleaned_data["document_number"],
                age=self.cleaned_data["age"],
                phone_number=self.cleaned_data["phone_number"],
                city=self.cleaned_data["city"],
                skill=self.cleaned_data["skill"],
                email=self.cleaned_data["email"],
            )
        else:
            new_empleador=Employer.objects.create(
                user=new_user,
                name=self.cleaned_data["name"],
                lastname=self.cleaned_data["lastname"],
                document_type=self.cleaned_data["document_type"],
                document_number=self.cleaned_data["document_number"],
                age=self.cleaned_data["age"],
                phone_number=self.cleaned_data["phone_number"],
                city=self.cleaned_data["city"],
                email=self.cleaned_data["email"],
            )


