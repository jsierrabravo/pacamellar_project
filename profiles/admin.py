from django.contrib import admin

# Register your models here.
from .models import DocumentType, City, Skill, Employee, Employer

admin.site.register(DocumentType)
admin.site.register(City)
admin.site.register(Skill)
admin.site.register(Employee)
admin.site.register(Employer)
