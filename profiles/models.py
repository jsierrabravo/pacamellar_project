from django.contrib.auth.models import AbstractUser, User
from django.db import models


# Create your models here.

class DocumentType(models.Model):
    document_type = models.CharField(max_length=5)

    def __str__(self):
        return self.document_type


class City(models.Model):
    city = models.CharField(max_length=25)

    def __str__(self):
        return self.city


class Skill(models.Model):
    skill = models.CharField(max_length=25)

    def __str__(self):
        return self.skill


class Employee(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=30)
    lastname = models.CharField(max_length=30)
    document_type = models.ForeignKey(DocumentType, on_delete=models.CASCADE)
    document_number = models.IntegerField(unique=True)
    age = models.IntegerField()
    phone_number = models.CharField(max_length=10)
    email = models.EmailField()
    # qualification = models.IntegerField()
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    skill = models.ForeignKey(Skill, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.name} {self.lastname}'


class Employer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=30)
    lastname = models.CharField(max_length=30)
    document_type = models.ForeignKey(DocumentType, on_delete=models.CASCADE)
    document_number = models.IntegerField(unique=True)
    age = models.IntegerField()
    phone_number = models.CharField(max_length=10)
    email = models.EmailField()
    # qualification = models.IntegerField()
    city = models.ForeignKey(City, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.name} {self.lastname}'
