from django.db import models

# Create your models here.
from profiles.models import Employee, Skill, Employer


class Status(models.Model):
    status = models.CharField(max_length=20)

    def __str__(self):
        return self.status


class Advertisement(models.Model):
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE)
    skill = models.ForeignKey(Skill, on_delete=models.CASCADE)
    title = models.CharField(max_length=50)
    description = models.CharField(max_length=250)
    experience = models.IntegerField(blank=True, null=True)
    status = models.ForeignKey(Status, on_delete=models.CASCADE)
    # publicated_at = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.title


class Necessity(models.Model):
    employer = models.ForeignKey(Employer, on_delete=models.CASCADE)
    skill = models.ForeignKey(Skill, on_delete=models.CASCADE)
    title = models.CharField(max_length=50)
    description = models.CharField(max_length=250)
    time_lapse = models.IntegerField()
    remuneration = models.FloatField()
    # qualification = models.IntegerField()
    status = models.ForeignKey(Status, on_delete=models.CASCADE)
    # publicated_at = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.title


class AdvertisementContract(models.Model):
    advertisement = models.ForeignKey(Advertisement, on_delete=models.CASCADE)
    employer = models.ForeignKey(Employer, on_delete=models.CASCADE)
    employee_qualification = models.IntegerField()
    employer_qualification = models.IntegerField()
    # publicated_at = models.DateField(auto_now_add=True)


class NecessityContract(models.Model):
    necessity = models.ForeignKey(Necessity, on_delete=models.CASCADE)
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE)
    employee_qualification = models.IntegerField()
    employer_qualification = models.IntegerField()
    # publicated_at = models.DateField(auto_now_add=True)
