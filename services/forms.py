from django import forms
from .models import Advertisement, Necessity, Status
from profiles.models import Employee, Skill, Employer
from django.forms import TextInput, Select, NumberInput, Textarea


class AdvertisementForm(forms.ModelForm):
    class Meta:
        model = Advertisement
        fields = ('employee', 'skill', 'title', 'description', 'experience', 'status')
        widgets = {
            'employee': Select(
                attrs={
                    'class': 'form-control mb-3',
                    'style': ''
                }
            ),
            'skill': Select(
                attrs={
                    'class': 'form-control',
                    'style': ''
                }
            ),
            'title': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Title'
                }
            ),
            'description': Textarea(
                attrs={
                    'type': 'text',
                    'class': 'form-control',
                    'placeholder': 'Description',
                    'rows': '6',
                    'cols': '50'
                }
            ),
            'experience': NumberInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Months of experience',
                    'min': '0'
                }
            ),
            'status': Select(
                attrs={
                    'class': 'form-control',
                    'style': ''
                }
            ),
        }


class NecessityForm(forms.ModelForm):
    class Meta:
        model = Necessity
        fields = ('employer', 'skill', 'title', 'description', 'time_lapse', 'remuneration', 'status')
        widgets = {
            'employer': Select(
                attrs={
                    'class': 'form-control mb-3',
                    'style': ''
                }
            ),
            'skill': Select(
                attrs={
                    'class': 'form-control',
                    'style': ''
                }
            ),
            'title': TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Title'
                }
            ),
            'description': Textarea(
                attrs={
                    'type': 'text',
                    'class': 'form-control',
                    'placeholder': 'Description',
                    'rows': '6',
                    'cols': '50'
                }
            ),
            'time_lapse': NumberInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Lapse of time in months',
                    'min': '0'
                }
            ),
            'remuneration': NumberInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Remuneration in COP',
                    'step': 'any',
                    'min': '0'
                }
            ),
            'status': Select(
                attrs={
                    'class': 'form-control',
                    'style': ''
                }
            ),
        }
