# Create your views here.
from django.shortcuts import render
from django.views.generic import ListView, UpdateView, DeleteView, CreateView

from .forms import AdvertisementForm, NecessityForm
from .models import Advertisement, Necessity


def home(request):
    return render(request, 'home.html')


class RegisterAdvertisementView(CreateView):
    model = Advertisement
    form_class = AdvertisementForm
    template_name = 'add_advertisement.html'
    success_url = '/advertisements_list'
    context_object_name = 'form'


class RegisterNecessityView(CreateView):
    model = Necessity
    form_class = NecessityForm
    template_name = 'add_necessity.html'
    success_url = '/necessities_list'
    context_object_name = 'form'


class AdvertisementsListView(ListView):
    model = Advertisement
    queryset = Advertisement.objects.all()
    template_name = 'advertisements_list.html'
    context_object_name = 'items'


class NecessitiesListView(ListView):
    # myemployer = list(Necessity.objects.all())[0].employer
    # myemployer_user = str(Employer.objects.filter(id=myemployer.id)[0].user)
    model = Necessity
    queryset = Necessity.objects.all()
    template_name = 'necessities_list.html'
    context_object_name = 'items'


class EditNecessityView(UpdateView):
    model = Necessity
    form_class = NecessityForm
    pk_url_kwarg = 'pk'
    template_name = 'add_necessity.html'
    success_url = '/necessities_list'


class EditAdvertisementView(UpdateView):
    model = Advertisement
    form_class = AdvertisementForm
    pk_url_kwarg = 'pk'
    template_name = 'add_advertisement.html'
    success_url = '/advertisements_list'


class DeleteNecessityView(DeleteView):
    model = Necessity
    pk_url_kwarg = 'pk'
    template_name = 'delete_necessity.html'
    success_url = '/necessities_list'


class DeleteAdvertisementView(DeleteView):
    model = Advertisement
    pk_url_kwarg = 'pk'
    template_name = 'delete_advertisement.html'
    success_url = '/advertisements_list'
