from django.contrib import admin

# Register your models here.
from .models import Status, Advertisement, Necessity, AdvertisementContract, NecessityContract

admin.site.register(Status)
admin.site.register(Advertisement)
admin.site.register(Necessity)
admin.site.register(AdvertisementContract)
admin.site.register(NecessityContract)

