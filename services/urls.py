from django.contrib.auth.decorators import login_required
from django.urls import path

from .views import EditNecessityView, EditAdvertisementView, DeleteNecessityView, DeleteAdvertisementView 
from .views import home, AdvertisementsListView, NecessitiesListView, RegisterAdvertisementView, RegisterNecessityView

urlpatterns = [
    path('home', home, name='home'),
    path('add_advertisement', login_required(RegisterAdvertisementView.as_view()), name='add_advertisement'),
    path('add_necessity', login_required(RegisterNecessityView.as_view()), name='add_necessity'),
    path('advertisements_list', login_required(AdvertisementsListView.as_view()), name='advertisements_list'),
    path('necessities_list', login_required(NecessitiesListView.as_view()), name='necessities_list'),
    path('edit_necessity/<int:pk>', login_required(EditNecessityView.as_view()), name='edit_necessity'),
    path('edit_advertisement/<int:pk>', login_required(EditAdvertisementView.as_view()), name='edit_advertisement'),
    path('delete_necessity/<int:pk>', login_required(DeleteNecessityView.as_view()), name='delete_necessity'),
    path('delete_advertisement/<int:pk>', login_required(DeleteAdvertisementView.as_view()), name='delete_advertisement'),

]
