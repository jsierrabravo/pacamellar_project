## Developers

* Juan Sebastián Sierra Bravo
* Angélica Patricia Morales Daza
* Jair Calderón Velaides

## Dependencies

* [git](https://git-scm.com/downloads)

## Installation

1. `git clone git@gitlab.com:jsierrabravo/pacamellar_project.git`
2. `cd pacamellar_project`
3. `pip install -r requirements.txt`
4. `python3 manage.py migrate`
5. `python3 manage.py runserver`
6. Go to `http://127.0.0.1:8000/`